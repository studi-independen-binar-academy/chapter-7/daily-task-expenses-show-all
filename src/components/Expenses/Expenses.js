import React, { useState } from 'react';

import ExpensesFilter from './ExpensesFilter';
import Card from '../UI/Card';
import './Expenses.css';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';
import ExpenseItem from './ExpenseItem';

const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('Show All')

  const filterChangeHandler = selectedYear => {
    setFilteredYear(selectedYear)
  }

  const filteredExpense = props.items.filter(expense => {
    if (filteredYear === 'Show All') {
      return (
        <ul className='expenses-list'>
          {props.items.map((expense) => (
            <ExpenseItem
              key={expense.id}
              title={expense.title}
              amount={expense.amount}
              date={expense.date}
            />))}
        </ul>
      )
    } else {
      return expense.date.getFullYear().toString() === filteredYear
    }

  })

  return (
    <div>
      <Card className='expenses'>
        <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
        <ExpensesChart expenses={filteredExpense} />
        <ExpensesList items={filteredExpense} />
      </Card>
    </div>
  );
};

export default Expenses;